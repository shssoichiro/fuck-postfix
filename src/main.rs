use std::env::args;

fn main() {
    let input = args().nth(1).unwrap();

    let items = input
        .split_whitespace()
        .map(|item| {
            if item == "?" {
                Item::IfElse
            } else if ["exp", "log", "sqrt", "abs", "not", "dup", "dupN"].contains(&item) {
                Item::SingleOperator(item)
            } else if [
                "+", "-", "*", "/", "max", "min", "pow", ">", "<", "=", ">=", "<=", "and", "or",
                "xor", "swap", "swapN",
            ]
            .contains(&item)
            {
                Item::DoubleOperator(item)
            } else {
                Item::Operand(item)
            }
        })
        .collect::<Vec<_>>();
    let mut stack = Vec::new();
    for item in items {
        match item {
            Item::Operand(_) => {
                stack.push(item);
            }
            Item::IfElse => {
                let var3 = stack.pop().unwrap();
                let var2 = stack.pop().unwrap();
                let var1 = stack.pop().unwrap();
                stack.push(Item::Expression(format!(
                    "({} ? {} : {})",
                    var1.as_ref(),
                    var2.as_ref(),
                    var3.as_ref()
                )));
            }
            Item::DoubleOperator(op) => {
                let var2 = stack.pop().unwrap();
                let var1 = stack.pop().unwrap();
                stack.push(Item::Expression(format!(
                    "({} {} {})",
                    var1.as_ref(),
                    op,
                    var2.as_ref()
                )));
            }
            Item::SingleOperator(op) => {
                let var1 = stack.pop().unwrap();
                stack.push(Item::Expression(format!("({} {})", op, var1.as_ref())));
            }
            _ => unreachable!(),
        }
    }
    dbg!(&stack);
    assert!(stack.len() == 1);
    eprintln!("{}", stack[0].as_ref());
}

#[derive(Debug, Clone)]
enum Item<'a> {
    Operand(&'a str),
    DoubleOperator(&'a str),
    SingleOperator(&'a str),
    Expression(String),
    IfElse,
}

impl AsRef<str> for Item<'_> {
    fn as_ref(&self) -> &str {
        match self {
            Item::Operand(item) => item,
            Item::DoubleOperator(item) => item,
            Item::SingleOperator(item) => item,
            Item::Expression(item) => item.as_str(),
            Item::IfElse => "",
        }
    }
}
